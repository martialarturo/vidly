namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'55777288-2892-4e59-a29d-98a288c09e11', N'guest@vidly.com', 0, N'AA5W4iDSkd6HgAHmxZunbk8JVLRq4W94bahaaA5f9i2mjfZfv24n8xmYKeDLuIN54Q==', N'36a0c295-e26e-49d2-ac0f-b4bddabd61cd', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8b2bf6ad-cdc6-417b-9f61-f1d51e02dd4f', N'admin@vidly.com', 0, N'AMiWhfjoefJwbYoCGfKy8igW0eTndXE3yySFdUQ5G54PVDKzv0PZC4ObPqk309OcAQ==', N'3fc29fc5-4f0d-4f41-971a-63f308e57dab', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'd266083d-12ff-49e5-a4f3-bbd8d592ca40', N'CanManageMovies')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8b2bf6ad-cdc6-417b-9f61-f1d51e02dd4f', N'd266083d-12ff-49e5-a4f3-bbd8d592ca40')

");
        }
        
        public override void Down()
        {
        }
    }
}
