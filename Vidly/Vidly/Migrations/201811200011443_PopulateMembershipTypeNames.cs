namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMembershipTypeNames : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.MembershipTypes SET MembershipTypeName = 'Pay As You Go' WHERE DurationInMonths = 0");
            Sql("UPDATE dbo.MembershipTypes SET MembershipTypeName = 'Monthly' WHERE DurationInMonths = 1");
            Sql("UPDATE dbo.MembershipTypes SET MembershipTypeName = 'Quarterly' WHERE DurationInMonths = 3");
            Sql("UPDATE dbo.MembershipTypes SET MembershipTypeName = 'Annual' WHERE DurationInMonths = 12");
        }

        public override void Down()
        {
        }
    }
}
