namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GiveBirthdayToCustomer2 : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.Customers SET Birthdate = '5/30/1986' WHERE Id = 2");
        }
        
        public override void Down()
        {
        }
    }
}
