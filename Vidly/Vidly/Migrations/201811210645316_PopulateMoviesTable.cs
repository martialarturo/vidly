namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMoviesTable : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO dbo.Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('Hangover', 1, '1/1/2010', '1/1/2000', 1)");
            Sql("INSERT INTO dbo.Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('Die Hard', 2, '2/2/2012', '2/1/2002', 2)");
            Sql("INSERT INTO dbo.Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('The Terminator', 2, '3/3/2013', '3/3/2003', 3)");
            Sql("INSERT INTO dbo.Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('Toy Story', 3, '4/4/2014', '4/4/2004', 4)");
            Sql("INSERT INTO dbo.Movies (Name, GenreId, DateAdded, ReleaseDate, NumberInStock) VALUES ('Titantic', 4, '5/5/2015', '5/5/2005', 5)");
        }
        
        public override void Down()
        {
        }
    }
}
