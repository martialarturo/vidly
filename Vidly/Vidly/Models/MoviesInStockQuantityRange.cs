﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class MoviesInStockQuantityRange : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var movie = (Movie)validationContext.ObjectInstance;
            var quantity = movie.NumberInStock;

            return (quantity >= 0 && quantity <= 20)
                ? ValidationResult.Success
                : new ValidationResult("Number In Stock must be between 1 and 20.");
        }
    }
}